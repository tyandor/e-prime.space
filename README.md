# E-Prime

An app for experimental writing in E-Prime. Available at [e-prime.space](https://e-prime.space)

Created by Tyler Andor.

## About

This app serves as an aid in writing action-oriented prose by calling out the use of be-verbs. It's really as simple as that. Write without using any be-verbs, and see what happens. 

The ideas behind E-Prime come from the anti-Aristotelian philosophy of Alfred Korzybski, founder of [General Semantics](https://www.wikiwand.com/en/General_semantics). Building upon his work, several linguists, David Bourland and Paul Johnston prominent among them, popularized E-Prime as a way of putting Korzybski's ideas into practice. More than just a writing exercise, E-Prime creates a different, more precise way of thinking about the world and the role of language in human understanding. 

Personally, I find the challenge of writing and thinking in E-Prime fruitful and enjoyable, even if it sometimes makes writing unbearable.

## Technology

E-Prime is built on React/Gatsby, with basic blog functionality borrowed from the [LekoArts Minimal Blog starter](https://github.com/LekoArts/gatsby-starter-minimal-blog). 

## Development

For local development you'll need [Node.js](https://nodejs.org/en/), the [Gatsby CLI](https://www.gatsbyjs.org/docs/) and [node-gyp](https://github.com/nodejs/node-gyp#installation) installed. The official Gatsby website also lists two articles regarding this topic:

- [Gatsby on Windows](https://www.gatsbyjs.org/docs/gatsby-on-windows/)
- [Check your development environment](https://www.gatsbyjs.org/tutorial/part-zero/)

Otherwise all you have to do to get up and running is clone this repo, cd into the directory, and run

```
npm install
npm run dev
```

That should open the app for you in your default browser at `http://localhost:8000/`

## Building & Deployment

To build a production version -- compiled to `/public` run

```
npm run build
```

Deployment is set up through Netlify's CI process, connected to version control on GitLab. 

## License

<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">E-Prime</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://tyandor.com" property="cc:attributionName" rel="cc:attributionURL">Tyler Andor</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br>Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="http://e-prime.space" rel="dct:source">https://e-prime.space</a>.

