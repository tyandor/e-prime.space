// JS Goes here - ES6 supported

// activate the cursor in the editor on load
document.getElementById("editor").focus();

// toggle between editor and checker
$('.checker').click(function() {
    $('#editor').hide();
    $('.editor').removeClass('active');
    $('.checker').addClass('active');
    $('#checker, #evaluate').show();
    document.getElementById("checker").focus();
});

$('.editor').click(function() {
    $('#checker, #evaluate').hide();
    $('.checker').removeClass('active');
    $('.editor').addClass('active');
    $('#editor').show();
    document.getElementById("editor").focus();
});

// activate/deactivate check button
document.getElementById("checker").addEventListener("input", function() {
    $('#evaluate').prop('disabled', false);
}, false);

// enable placeholder text
jQuery(function($){
    $("#editor").focusout(function(){
        var element = $(this);        
        if (!element.text().replace(" ", "").length) {
            element.empty();
        }
    });
});

// strip away styling from the user's clipboard on paste
editor.addEventListener("paste", function(e) {
    // cancel paste
    e.preventDefault();
    // get text representation of clipboard
    var text = e.clipboardData.getData("text/plain");
    // insert text manually
    document.execCommand("insertHTML", false, text);
});

var changed,
    lastValue = '',
    div = $('#editor'),
    words = ['am', 'is', 'isn\'t', 'are', 'aren\'t', 'was', 'wasn\'t', 'were', 'weren\'t', 'be', 'being', 'been', 'it\'s', 'you\'re', 'they\'re', 'we\'re', 'I\'m', 'he\'s', 'she\'s', 'there\'s', 'here\'s', 'where\'s', 'how\'s', 'what\'s', 'who\'s', 'that\'s', 'ain\'t', 'hain\'t', 'whatcha', 'yer'];

function markWords() {
    var html = div.html().replace(/<\/?strong>/gi, ''),
        text = html.replace(/<[^>]+>/g, ' ').replace(/\s+/g, ' '),
        exp;
    $.each(words, function(i, word) {
        exp = new RegExp('\\b(' + word + ')\\b', 'gi');
        html = html.replace(exp, function(m) {
            console.log('WORD MATCH:', m);
            return '<strong>' + m + '</strong>';
        });
    });
    div.html(html);
}

setInterval(function() {
    var html = div.html();
    if ( lastValue != html && html ) {
        //console.log(lastValue);
        //console.log(html);
        lastValue = html;
        markWords();
        setEndOfContenteditable(div[0]);
    }
}, 250);

function setEndOfContenteditable(contentEditableElement) {
    var range, selection;
    if(document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
    {
        range = document.createRange();//Create a range (a range is a like the selection but invisible)
        range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
        range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
        selection = window.getSelection();//get the selection object (allows you to change selection)
        selection.removeAllRanges();//remove any selections already made
        selection.addRange(range);//make the range you have just created the visible selection
    }
    else if(document.selection)//IE 8 and lower
    { 
        range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
        range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
        range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
        range.select();//Select the range (make it the visible selection
    }
}

var checkdiv = $('#checker');

function checkWords() {
    var html = checkdiv.html().replace(/<\/?strong>/gi, ''),
        text = html.replace(/<[^>]+>/g, ' ').replace(/\s+/g, ' '),
        exp;
    $.each(words, function(i, word) {
        exp = new RegExp('\\b(' + word + ')\\b', 'gi');
        html = html.replace(exp, function(m) {
            console.log('WORD MATCH:', m);
            return '<strong>' + m + '</strong>';
        });
    });
    checkdiv.html(html);
}

$('#evaluate').click(function() {
    checkWords();
});

